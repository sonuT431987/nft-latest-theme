import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import classes from './AllAssets001.module.scss';
import Buttons from '../../../public/account/Buttons.module.scss';


import Modal from 'react-bootstrap/Modal';


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faEye, faFlag, faUser, faWallet } from '@fortawesome/free-solid-svg-icons';
library.add(faFlag, faEye, faUser, faBitbucket, faWallet,);


import paginate from '../../../public/account/Pagination.module.scss';
import Form from '../../../public/account/Form.module.scss';
import Modals from '../../../public/account/Modals.module.scss';
import Thumbs from '../../../public/dummyassets/thumbs.png';
import { faBitbucket } from "@fortawesome/free-brands-svg-icons";


export default function AllAssets001(props) {
    const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);



	return (
		<>
			<div className={`Wraps ${classes.assetsAll}`}>

				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
				<article className={classes.thumbsWrap} onClick={handleShow}>
				<Link className={classes.thumbsItem} href="">
					<a>
					<Image
						src={Thumbs}
						height={512}
						width={512}
						alt="Thumbs"
					/>
					</a>
				</Link>
				</article>
			</div>
			<div className={paginate.paginationWrap}>
				<div className={Form.totalFilter}>
					<select className={`form-select ${Form.selectDrop}`}>
						<option>10</option>
						<option>11</option>
						<option>12</option>
					</select>
					<span className={Form.statusCount}>490 Assets</span>
				</div>
				<div className={paginate.paginate}>
					<a href="#" className={paginate.activate}>1</a>
					<a href="#">2</a>
					<a href="#">...</a>
					<a href="#">10</a>
					<a href="#">11</a>
					<a href="#">12</a>
					<a href="#">26</a>
					<a href="#">Next</a>
				</div>
			</div>

			<Modal size="lg" show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                    <div className='row'>
                        <div className='col-sm-6'>
                        <Image
                            src={Thumbs}
                            height={384}
                            width={472}
                            alt="Thumbs"
                        />
                        </div>
						<div className="col-sm-6">
							<h1 className={Modals.bodyTitles}>Mixed Space</h1>
							<p className={Modals.listedTime}>Listed 1 day ago</p>
							<div className={Modals.listOne}><FontAwesomeIcon icon={faUser} size="1x" /> <span>@JennyWilson</span></div>
							<div className={Modals.listWrap}><span><FontAwesomeIcon icon={faBitbucket} size="1x" />Bitkub Chain</span> <span><FontAwesomeIcon icon={faWallet} size="1x" /> 0xceB945...Bd3c8D5D</span></div>
							<h4 className={Modals.descHead}>Description</h4>
							<p className={Modals.para}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book...see more</p>
							<div className={Modals.tags}><span>Collection:</span> <Link href="/"><a>Collection1</a></Link>, <Link href="/"><a>Collection2</a></Link></div>
							<div className={Modals.tags}><span>Exhibition:</span> <Link href="/"><a>Exhibition1</a></Link>, <Link href="/"><a>Exhibition2</a></Link></div>
							<div className={Modals.share}>
								<span><FontAwesomeIcon icon={faEye} size="1x" /> 999K</span>
								<span><FontAwesomeIcon icon={faFlag} size="1x" /> Report</span>
							</div>
							<div><button type="button" className={`btn w-100 ${Buttons.btnPrimary}`}>VIEW  IN MARKET</button> </div>
						</div>
                    </div>
            </Modal.Body>
        </Modal>
	</>
  );
}

