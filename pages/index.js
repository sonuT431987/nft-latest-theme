import React, { useEffect } from "react";
import Link from "next/link";
import Buttons from '../public/account/Buttons.module.scss';



import { useWeb3 } from "@3rdweb/hooks";

export default function Home() {
	useEffect( () => { document.querySelector("body").classList.add("mainHome") } );
	const { address, connectWallet, disconnectWallet } = useWeb3();
	return (
	<>
		<div className="container">
			<div className="contentwrap">
				
				
				<div className="text-end p-4">
				<ul className="list-unstyled walletwrap">
					<li>
						<Link href="/">
						{address ? (
							<div>
							<span className="mataaddress">Hello, <span className="walletID">{address.substring(0, 5)}…{address.substring(address.length - 4)}</span></span>
								<Link href="/">
								<a className={`btn ${Buttons.btnDanger}`} onClick={() => disconnectWallet("injected")}>Disconnect</a>
								</Link>	
							</div>
						) : (
							<Link href="/">
							<a className={`btn ${Buttons.btnPrimary}`} onClick={() => connectWallet("injected")} >
								Connect Wallet
							</a>
							</Link>
						)}
						</Link>
					</li>
					</ul>
				</div>
			</div>
		</div>
	</>
  )
}

