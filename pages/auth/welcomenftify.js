import React, {useEffect} from 'react';
import AuthStyle from './AuthStyle.module.scss';
import Buttons from '../../public/account/Buttons.module.scss';
import Image from "next/image";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faClose } from '@fortawesome/free-solid-svg-icons';
library.add(faAngleLeft);


import Slider from "react-slick";


import SlickImg from '../../public/main/images/thumbsfull.png';




const WalletConnect = () => {
	useEffect( () => { document.querySelector("body").classList.add("AuthWrap") } );

	const sliderSettings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
	};

  return (
    <>
		<div className={AuthStyle.headerRight}>
			<div className='container'>
				<div className={AuthStyle.closeBtn}><FontAwesomeIcon icon={faClose} size="lg" /></div>
				
			</div>
		</div>
		<div className={AuthStyle.wraper}>
			<h1 className={AuthStyle.head}>Welcome to NFTIFY</h1>
			<h5 className={AuthStyle.subHead}>Are you ready to experience of NFT?<span className="lineBreak"></span> Select your favourite profile theme.</h5>
			<div className={AuthStyle.sliderList}>
				<Slider {...sliderSettings}>
					<div className={AuthStyle.thumbsWrap}>
						<Image src={SlickImg} alt='Logo' width={512} height={376} />
						<div className={AuthStyle.caption}>Default</div>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<Image src={SlickImg} alt='Logo' width={512} height={376} />
						<div className={AuthStyle.caption}>Default</div>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<Image src={SlickImg} alt='Logo' width={512} height={376} />
						<div className={AuthStyle.caption}>Default</div>
					</div>
					<div className={AuthStyle.thumbsWrap}>
						<Image src={SlickImg} alt='Logo' width={512} height={376} />
						<div className={AuthStyle.caption}>Default</div>
					</div>
				</Slider>
			</div>
			<div className={AuthStyle.btnWrap}>
				<button type='button' className={`btn ${Buttons.btnPrimary}`}>NEXT</button>
			</div>
		</div>
    </>
  );
}

export default WalletConnect;



