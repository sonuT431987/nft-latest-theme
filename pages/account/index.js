import Image from "next/image";
import React, { useEffect } from "react";
import LeftSidebar from "../../components/account/layout/leftsidebar";
import Form from '../../public/account/Form.module.scss';
import PagesAuth from '@/layout/PageAuth';
import Buttons from '../../public/account/Buttons.module.scss';

import profileLogo from '../../public/dummyassets/profile.png';

import classes from '../../public/account/Index.module.scss';


import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faArrowRightFromBracket, faCheck, faClone, faPlus } from '@fortawesome/free-solid-svg-icons';
library.add(faCheck, faClone, faAngleLeft, faArrowRightFromBracket, faPlus);



import {
    faFacebook,
    faTwitter,
    faInstagram
  } from "@fortawesome/free-brands-svg-icons";


const Index = (props) => {
	useEffect(() => {
		document.querySelector("body").classList.add("userBg");
	});
  return ( 
    <>
	<PagesAuth route={props.route}>
      <div className='userWrap'>		  
		<div className='container'>
			<div className='row'>
				<div className='col-md-3'>
					<LeftSidebar />
				</div>
				<div className='col-md-9'>
					<h3 className={classes.accHead}>My Account</h3>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Profile Image</label>
						<div className="col-md-9">
							<div className={classes.uploadWrap}>
								<label className={classes.labelHead}>Select Image</label>
								<ul className={classes.uploadList}>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li><Image className={classes.profileImg} src={profileLogo} width={100} height={100} alt="Profile Image" /></li>
									<li className={classes.uploadFile}><input type="file" /><FontAwesomeIcon icon={faPlus} size="3x" /></li>
								</ul>
								<div className={classes.listNote}>Recommended image size: 1600 x 360px</div>
								<hr className={classes.lineHr} />
								<label className={classes.labelHead}>Decide how to crop your image</label>
								<ul className="row list-unstyled">
									<li className="col-sm-3">
										<div className={classes.uploadThumb}>
											<div className={classes.thumbWrap}>
												<Image className={classes.verticallyImg} src={profileLogo} width={50} height={50} alt="Profile Image" />
											</div>
											<div className={classes.captionName}>Fill the frame vertically</div>
										</div>
									</li>
									<li className="col-sm-3">
										<div className={classes.uploadThumb}>
											<div className={classes.thumbWrap}>
												<Image className={classes.horizantalImg} src={profileLogo} width={150} height={50} alt="Profile Image" />
											</div>
											<div className={classes.captionName}>Fill the frame horizontally</div>
										</div>
									</li>
									<li className="col-sm-3">
										<div className={classes.uploadThumb}>
											<div className={classes.thumbWrap}>
												<Image className={classes.coverImg} src={profileLogo} width={100} height={100} alt="Profile Image" />
											</div>
											<div className={classes.captionName}>Cover the frame</div>
										</div>
									</li>
									<li className="col-sm-3">
										<div className={classes.uploadThumb}>
											<div className={classes.thumbWrap}>
												<Image className={classes.originalImg} src={profileLogo} width={100} height={100} alt="Profile Image" />
											</div>
											<div className={classes.captionName}>Original size</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Display Name</label>
						<div className="col-md-6">
							<input type="text" className={`form-control ${Form.formElements}`} placeholder="Jenny Wilson" />
						</div>
					</div>
					<div className="mb-3 row align-items-center">
						<label className="col-md-3 col-form-label">Username</label>
						<div className="col-sm-6">
							<div className="input-group flex-nowrap">
								<input type="text" className={`form-control ${Form.formElements}`} placeholder="Username" />
								<span className="input-group-text"><FontAwesomeIcon icon={faCheck} className="checkTrue" size="lg" /></span>
							</div>
						</div>
						<div className="col-sm-1">
							<button type='button' className='btn btn-link'>Edit</button>
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Connected Wallet</label>
						<div className="col-md-9">
							<div className="input-group flex-nowrap">
								<input type="text" className={`form-control ${Form.formElements}`} placeholder="0xf7A81...613Ff" />
								<span className="input-group-text"><FontAwesomeIcon icon={faClone} className="faClone" size="lg" /></span>
							</div>
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-md-3 col-form-label">Bio</label>
						<div className="col-md-9">
							<textarea className={`form-control ${Form.formElements}`} placeholder='Tell your fans about your story...'></textarea>
							<div className={classes.charCount}>0/500</div>
						</div>
					</div>
					<hr />
					<h3 className={classes.accHead}>Connect with you</h3>
					<p className={classes.headDesc}>Add  your social networks to allow your fans to connect with you!</p>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faFacebook} size="lg" /> <span className={classes.socialText}>Facebook</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faTwitter} size="lg" /> <span className={classes.socialText}>Twitter</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<div className="mb-3 row">
						<label className="col-sm-4 col-form-label"><FontAwesomeIcon icon={faInstagram} size="lg" /> <span className={classes.socialText}>Instragram</span></label>
						<div className="col-sm-8">
							<input type="password" className={`form-control ${Form.formElements}`} placeholder='URL link' />
						</div>
					</div>
					<hr />
					<h3 className={classes.accHead}>Deactivate account</h3>
					<p className={classes.headDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<div className={classes.btnGroup}>
						<button type='button' className={`btn ${Buttons.btnOutlineDanger}`}>Deactivate</button>
						<div className={classes.btnsWrap}>
							<button type='button' className={`btn ${Buttons.btnOutlinePrimary}`}>Cancel</button>
							<button type='button' className={`btn ${Buttons.btnPrimary}`}>Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>
	  
	</PagesAuth>
    </>
  );
}
export default Index;