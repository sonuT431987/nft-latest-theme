import React, {useState} from 'react';
import Image from "next/image";
import AllAssets001 from '@/components/account/allassets';
import LeftSidebar from "../../components/account/layout/leftsidebar";
import PagesAuth from '@/layout/PageAuth';
import Form from '../../public/account/Form.module.scss';
import classes from '../../public/account/allassets.module.scss';


import Diamonds from '../../public/dummyassets/diamond.svg';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faSearch } from '@fortawesome/free-solid-svg-icons';
library.add(faSearch);



const AllAssets = (props) => {

  return (
    <>
		<PagesAuth route={props.route}>
			<div className='container'>
				<div className='row'>
					<div className='col-md-3'>
						<LeftSidebar />
					</div>
					<div className='col-md-9'>
						<h3 className={classes.head}>All Assets</h3>
						<p className={classes.headNote}>All assets from your connected wallet will display here.</p>
						<h4 className={classes.subHead}>Your wallet  <span><Image src={Diamonds} width={14} height={20} /> 0xf7A81...613Ff</span></h4>
						<div className='row'>
						<div className={`col-md-4 ${Form.formGroup}`}>
								<div className={`input-group ${Form.inputGroup}`}>
									<span className={`input-group-text ${Form.inputGroupText}`}><FontAwesomeIcon icon={faSearch} size="sm" /></span>
									<input type="text" className={` input-group ${Form.formElements}`} placeholder='Search by NFT name...' />
								</div>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
							<select className={`form-select ${Form.selectDrop}`}>
								<option>10</option>
								<option>11</option>
								<option>12</option>
							</select>
							</div>
							<div className={`col-md-3 ${Form.formGroup}`}>
								<select className={`form-select ${Form.selectDrop}`}>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
							<div className={`col-md-2 ${Form.formGroup}`}>
								<select className={`form-select ${Form.selectDrop}`}>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
						</div>
						<AllAssets001 />
					</div>
				</div>
			</div>
		</PagesAuth>
    </>
  );
}

export default AllAssets;
